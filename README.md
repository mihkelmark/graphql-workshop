# Docs

**GraphQL learning resources**
* GraphQL general spec: https://graphql.org/learn/
* Apollo server: https://www.apollographql.com/docs/apollo-server/
* Apollo client (React): https://www.apollographql.com/docs/react/
* Apollo client (iOS): https://www.apollographql.com/docs/react/



**Workshop API endpoints**

* GET `http://192.168.88.29:4002/films` 
* GET `http://192.168.88.29:4002/films/:id`
* GET `http://192.168.88.29:4002/directors`
* GET `http://192.168.88.29:4002/directors/:id`